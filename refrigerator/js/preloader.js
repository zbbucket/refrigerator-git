/*jslint browser:true */
/*global $, jQuery, createjs*/
/*jslint nomen: true */
/*jslint plusplus: true */
"use strict";
//==============================================================================
//                          resource load
//==============================================================================

function createPreloader() {
    
    //var _imageLocations = [
    //    '/images/refrigerator.png',
    //    '/images/lock_header.png',
    //    '/images/switch.png',
    //    '/images/pin_opened.png',
    //    '/images/pin_closed.png',
    //    '/images/hook.png',
    //    '/images/menu1.png',
    //    '/images/menu2.png',
    //    '/images/menu3.png'];
    
    function getImagesToLoad() {
        var srcOfImages = $('#imagesContainer img').map(function () {
            return $(this).attr('src');
        }).get();
            
        return srcOfImages;
    }
    
    function preloadData(finishCallback) {
        var queue = new createjs.LoadQueue(true),
            audioQueue = new createjs.LoadQueue(true),
            images = getImagesToLoad(),
            audioFiles,
            audioEntry,
            i;
        
        
        
        // 1. Set callback to launch application.
        queue.on('complete', function () {
            finishCallback();
        });
        
        
        // 2. Load images
        for (i = 0; i < images.length; i++) {
            queue.loadFile(images[i]);
        }
        
        // 3. Load sound.
        // We need separate queue. The loading will not be stuck, if audio
        // is not available for some reason.
        // We don't wait for them to start the game.

        audioFiles = [
            ['/sound/click-178186_33044-lq.ogg', 'click'],
            ['/sound/closed_door-12740_34346-lq.ogg', 'closedDoor'],
            ['/sound/opened_door-219490_4056007-lq.ogg', 'openedDoor'],
            ['/sound/shuffle-19245_9190-lq.ogg', 'shuffle'],
            ['/sound/latch-119494_1819253-lq.ogg', 'latch']
        ];

        createjs.Sound.alternateExtensions = ["mp3"];
        audioQueue.installPlugin(createjs.Sound);
        for (i = 0; i < audioFiles.length; i++) {
          audioEntry = audioFiles[i];
           
           audioQueue.loadFile({src: audioEntry[0], id: audioEntry[1]});
        }
    }
    
    
    var preloader = {};
    
    
    preloader.preloadData = preloadData;
    
    return preloader;
}