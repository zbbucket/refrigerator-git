/*jslint browser:true */
/*global $, jQuery*/
/*jslint nomen: true */
/*jslint plusplus: true */
"use strict";

function createStorage(utils) {
    function saveIsMute(isMute) {
        $.cookie.json = true;
        $.cookie('isMute', isMute);
    }
    
    function loadIsMute(isMute) {
        $.cookie.json = true;
        
        var result = $.cookie('isMute');
        
        return result === true;
    }
    
    function saveField(fieldAs2dArray, size) {
        $.cookie.json = true;
        
        
        var i,
            j,
            data = utils.createArray(size);
        
        for (i = 0; i < size; i++) {
            data[i] = utils.createArray(size);
            for (j = 0; j < size; j++) {
                data[i][j] = fieldAs2dArray[i][j];
            }
        }
        
        $.cookie('fieldSize', size);
        $.cookie('fieldState', data);
    }
    
    
    function loadField() {
        $.cookie.json = true;
        
        var size = $.cookie('fieldSize'),
            storedFieldState = $.cookie('fieldState'),
            resultFieldState = null,
            i,
            j,
            v,
            result,
            invalidResult = {isValid: false, size: '-1', field: null};
       
        if (typeof (size) === 'undefined' || size < 1 || size > 10) {
            return invalidResult;
        }
        
        
        try {
            resultFieldState = utils.createArray(size);
        
            for (i = 0; i < size; i++) {
                resultFieldState[i] = utils.createArray(size);
                for (j = 0; j < size; j++) {
                    v = parseInt(storedFieldState[i][j], 10);
                    resultFieldState[i][j] = v;
                    
                    if (isNaN(v)) {throw new Error('Cookie has invalid data'); }
                }
            }
        } catch (e) {
            // Cookie has invalid data, can't use it
            return invalidResult;
        }
            
        
        result = invalidResult;
        result.isValid = true;
        result.size = size;
        result.field = resultFieldState;
        
        return result;
    }
    
    var storage = {};
    
    storage.saveField = saveField;
    storage.loadField = loadField;
    
    storage.loadIsMute = loadIsMute;
    storage.saveIsMute = saveIsMute;
    
    return storage;
}
