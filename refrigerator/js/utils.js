/*jslint browser:true */
/*global $, jQuery*/
/*jslint nomen: true */
/*jslint plusplus: true */
"use strict";
function createUtils() {
    var utils = {};
    
    utils.translation = {
        'Done' : 'Готово! Дерните за рычаг, чтобы открыть дверь',
        'Not solved yet' : 'Заперто',
        'Open all pins' : 'Откройте все штифты',
        
        'Sound: off': 'Звук: выкл.',
        'Sound: on': 'Звук: вкл.',
        'Shuffle': 'Смешать',
        'Info': 'Досье'
    };

    utils._ = function (s) {
        var res = utils.translation[s];
    
        if (res === null) {
            res = s;
        }
        
        return res;
    };
    
    utils.createArray = function (size) {
        var array = new Array(size),
            i;
        
        for (i = 0; i < size; i++) {
            array[i] = 0;
        }
        
        return array;
    };

    utils.createArray2d = function (size) {
        var i,
            result = utils.createArray(size);
        
        for (i = 0; i < size; i++) {
            result[i] = utils.createArray(size);
        }
        
        return result;
    };
    
    return utils;
}