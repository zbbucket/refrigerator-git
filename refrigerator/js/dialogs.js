/*jslint browser: true*/
/*global $, jQuery, alert*/
"use strict";



function createDialogs() {
    function showInfoWindow() {
        $("#infoDiv").dialog({modal: true, title: 'Досье', minWidth: 620, minHeight: 550});
    }
    
    
    function showYoutubeWindow() {
        $('#priseLink').click();
    }
    
    function initYoutubeLink() {
        var titleText = 'Приз',
            text = $('#priseDivText').html();
        
        
        $(function () {
            $("a.youtube").YouTubePopup({ autoplay: 0, headerText: text, title: titleText, start: 152});
        });
    }
    
    function showLoadingMessage() {
        var loadingMessageDiv = document.getElementById('loadingMessageDiv');
        loadingMessageDiv.style.display = 'block';
    }
    
    function hideLoadingMessage() {
        var loadingMessageDiv = document.getElementById('loadingMessageDiv');
        loadingMessageDiv.style.display = 'none';
    }

    
    
    initYoutubeLink();

    $('#infoButton').click(function (evt) {
        showInfoWindow();
    });
    
    
    var dialogs = {};
    dialogs.showYoutubeWindow = showYoutubeWindow;
    dialogs.showInfoWindow = showInfoWindow;
    dialogs.showLoadingMessage = showLoadingMessage;
    dialogs.hideLoadingMessage = hideLoadingMessage;

    return dialogs;
}

