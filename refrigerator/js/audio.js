/*jslint browser:true */
/*global $, jQuery, createjs*/
/*jslint nomen: true */
/*jslint plusplus: true */
"use strict";
//=============================================================================
//                              Audio
//=============================================================================
function createAudio(storage) {
    var audio = {},
        isStorageEnabled = !!storage;
    
    audio.isMute = function () {
        return createjs.Sound.getMute();
    };
    
    audio.setMute = function (isMute) {
        createjs.Sound.setMute(isMute);
        if (isStorageEnabled) {storage.saveIsMute(audio.isMute()); }
    };
    
    audio.changeMute = function () {
        audio.setMute(!audio.isMute());
    };
    
    audio.play = function (soundId) {
        if (audio.isMute()) {return; }
        
        createjs.Sound.play(soundId);
    };
    
 
    if (isStorageEnabled) {audio.setMute(storage.loadIsMute()); }
    
    
    return audio;
}


