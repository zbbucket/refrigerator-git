/*jslint browser:true */
/*global $, jQuery, createjs*/
/*jslint nomen: true */
/*jslint plusplus: true */

"use strict";






//==============================================================================
//                          Init graphics
//==============================================================================

function createGraphics(field, utils, dialogs, audio) {
    var graphics = {},
        
        tweensCount = 0,
        activeTweeningObjects = {},
        
        isShuffling = false,
        shuffleSteps = 5,
        
        _;
    
    _ = utils._;
        
    //==============================================================================
    //                                   Utils
    //==============================================================================

    
    function play(sound) {
        audio.play(sound);
    }
    
    //==============================================================================
    //                                   Callbacks
	//==============================================================================
    
    function setSoundStateText() {
        var newText, textObject;
        if (audio.isMute()) {
            newText = _('Sound: off');
            
        } else {
            newText = _('Sound: on');
        }
        
        textObject = graphics.soundSettingsTextBox;
        textObject.text = newText;
        graphics.stage.update();
    }
    
    
    function showInfo() {
        dialogs.showInfoWindow();
    }
    
    function prise() {
        dialogs.showYoutubeWindow();
    }
    
    function alterSoundEnabled() {
        audio.changeMute();
        setSoundStateText();
    }
    
    

	//==============================================================================
	//                          Platform methods
	//==============================================================================

	function moveToOnCanvas(obj, x, y) {
	    obj.x = x;
	    obj.y = y;
	}

	function createImage(imageName) {
	    var bitmap = new createjs.Bitmap(imageName);
	    bitmap.regX = bitmap.image.width / 2;
	    bitmap.regY = bitmap.image.height / 2;
	    
	    return bitmap;
	}

	function createCenteredImageAtPosition(imagePath, x, y) {
		var image = createImage(imagePath);

		moveToOnCanvas(image, x, y);

		graphics.stage.addChild(image);

		return image;
	}

	function addText(text, posX, posY) {
	    var textObject = new createjs.Text(text, "25px Calibri", "green");

	    moveToOnCanvas(textObject, posX, posY);
	    
	    graphics.stage.addChild(textObject);
	    
	    return textObject;
	}

	function includeTransparentPartsToHitArea(bitmap) {
	    var hit = new createjs.Shape();
	    hit.graphics.beginFill("#000").rect(0, 0, bitmap.image.width, bitmap.image.height);
	    bitmap.hitArea = hit;
	}
	//-----------------------------------------------------------------------------
	//								Animation
	//-----------------------------------------------------------------------------
	
    function startStageTicker() {
	    createjs.Ticker.setFPS(60);
	    createjs.Ticker.addEventListener("tick", graphics.stage);
	}

	function stopStageTicker() {
	    createjs.Ticker.removeEventListener("tick", graphics.stage);
	}



	function tweenAnimation(obj, params, time, isOverride, callback) {
        // We count all active animations. When all of them are done,
        // we disable 60 FPS stage redraw.
        
        // However, if we requested new animation while the old one is still
        // acting, the handleComplete will be called only once, after last
        // animation is finished.
        
        // So we don't want to increase tweensCount variable in this case,
        // and we set additional flag as property of income object.
        if (obj.hasActiveTweenRefrigerator !== true) {
            tweensCount += 1;
            obj.hasActiveTweenRefrigerator = true;
        }
	    
	    
	    function handleComplete() {
	        tweensCount -= 1;
            
            // Animation is requested for switches. They are always the
            // same objects, so we don't remove this property.
            obj.hasActiveTweenRefrigerator = false;

	        // Last animation, no need to redraw scene anymore
	        if (tweensCount <= 0) {stopStageTicker(); }

	        if (typeof (callback) !== 'undefined') {callback(); }
	    }
	    
	    var tweenParams = {};
	    if (isOverride) {tweenParams.override = true; }
	    
	    // Launch the animation
	    createjs.Tween
            .get(obj, tweenParams)
            .to(params, time)
            .call(handleComplete);
	    
	    
	    // The first animation
	    if (tweensCount === 1) {startStageTicker(); }
	}



	//==============================================================================
	//                          All graphical elements
	//==============================================================================

	function createRefrigerator() {
	    var image = new createjs.Bitmap("/images/refrigerator.png");
	    graphics.stage.addChild(image);
	}



	function createPins() {
	    var i, pin;

	    // Pin has two images, full/shorted for closed/opened state.
	    // Normally the 'full' image overshadows 'shorted' one, and we
	    // see a long pin in a lock. When it is opened, the 'full' image is hidden,
	    // and we see only a short stick that don't prevent the door opening.

	    for (i = 0; i < graphics.field.size; i++) {
            graphics.pinsOpened[i] = createCenteredImageAtPosition('/images/pin_opened.png',
                106 + 29 * i  + 9, 60 + 47  -1);

            graphics.pinsClosed[i]  = createCenteredImageAtPosition('/images/pin_closed.png',
                106 + 29 * i + 10, 84 - 2);
	    }
	}

	function createLockHeader() {
	    var header = createCenteredImageAtPosition('/images/lock_header.png', 163, 45);
	}

	function createDoorHandle() {
	    var hook = createCenteredImageAtPosition('/images/hook.png', 33, 231 + 7);
	    
	    hook.on('click', function (evt) {
	        if (graphics.field.checkIsFinished()) {
	            
	            play('openedDoor');
	            
	            prise();
	        
	        } else {
	            play('closedDoor');
	        }
	        
	    });
	}

	
    
    //==============================================================================
    //                            Graphics of game logic
	//==============================================================================

    function rotateSwitchToNewPosition(i, j, isOverflow, isAnimated) {
        var sw = graphics.switches[i][j],
            newAngle;

        if (isOverflow) {sw.rotation -= 360; }
        
        newAngle = graphics.field.switchStates[i][j] * 90;

        if (isAnimated) {
            tweenAnimation(sw, {rotation: newAngle}, 100, true);
        } else {
            sw.rotation = newAngle;
        }
    }
    
    
    function getRotateSwitchFunction(isAnimated) {
        // Just a wrapper to get animated/static rotation.
        var isRotate = function (i, j, isOverflow) {
            rotateSwitchToNewPosition(i, j, isOverflow, isAnimated);
        };
        
        return isRotate;
    }

	function rotateUiRowAndCol(i, j, isAnimated) {
	    graphics.field.rotateColAndRow(i, j, getRotateSwitchFunction(isAnimated));
	}


	function updateGraphicsOfPinWithNumber(n) {
	    var pin = graphics.pinsClosed[n];
	    
	    if (graphics.field.isOpenedPinWithNumber(n)) {
	        pin.visible = false;
	    } else {
	        pin.visible = true;
	    }
	}

    
    
	function updateLockStatusText() {
	    var isDone = graphics.field.checkIsFinished(),
	        $shortMessageDiv = graphics.$shortMessageDiv;
	    if (isDone) {
	        $shortMessageDiv.text(_("Done"));
	        $shortMessageDiv.css('color', 'green');
	    } else {
	        $shortMessageDiv.text(_("Not solved yet"));
	        $shortMessageDiv.css('color', 'red');
	    }
	}
    
	function updatePins() {
	    var i;
	    for (i = 0; i < graphics.field.size; i++) {
	        updateGraphicsOfPinWithNumber(i);
	    }
	     
	    updateLockStatusText();
	}

	function initSwitchGraphicsAngle(sw) {
        rotateSwitchToNewPosition(sw.i, sw.j, false, false);

        graphics.stage.update();
	}
    
    

	function createSwitch(i, j) {
	    var sw = createCenteredImageAtPosition('/images/switch.png', 85 + i * 40, 160 + j * 40 );

	    sw.i = i;
	    sw.j = j;
	            
	    
	    includeTransparentPartsToHitArea(sw);
	    
	    
	    
	    graphics.switches[i][j] = sw;
        
        
        initSwitchGraphicsAngle(sw);
	    
	    
	    sw.on('click', function (evt) {
	        rotateUiRowAndCol(sw.i, sw.j, true);
	        updatePins();
	        
	        graphics.stage.update();
	        
	        graphics.field.save();
	        
	        if (graphics.field.checkIsFinished()) {
	            play('latch');
	        } else {
	            play('click');
	        }
	    });

	    return sw;
    }


	function initField() {
	    var i, j, sw, size;
	    size = graphics.field.size;
	    for (i = 0; i < size; i++) {
	        for (j = 0; j < size; j++) {
	            sw = createSwitch(i, j);
	            graphics.stage.addChild(sw);
	        }
	    }
        
        updatePins();
        
	    graphics.stage.update();
	}
    



	//-----------------------------------------------------------------------------
    //                          Shuffle
    //-----------------------------------------------------------------------------

    function doShuffle(isAnimated) {
        graphics.field.shuffle(getRotateSwitchFunction(isAnimated));
        
        updatePins();
    }

    function shuffleWithoutAnimation() {
        var i;
        for (i = 0; i < shuffleSteps; i++) {
            doShuffle(false);
        }
    }

    function shuffle() {
        // Prevent audio stack overflow
        if (isShuffling) {return; }
        isShuffling = true;
        
        
        var timesToShuffle = shuffleSteps,
            interval;
        
        // 1. Start a long-playing sound effect.
        play('shuffle');
    
    
        // 2. Start first shuffle animation.
        timesToShuffle -= 1;
        doShuffle(true);
        
    
    
        interval = setInterval(function () {
            // 3. Animate shuffle every 80 ms.
            timesToShuffle -= 1;
            doShuffle(true);
            
            // 4. Repeat it only (shuffleSteps-1) times       
            if (timesToShuffle < 0) {
                clearInterval(interval);
                isShuffling = false;
                
                graphics.field.save();

                
            }
            
            
        }, 80);
    }
	//==============================================================================
    //                               Menu
	//==============================================================================

	function createMenu() {
	    var menu1, menu2, menu3,
	        soundText, shuffleText, infoText;
	    menu1 = createCenteredImageAtPosition('/images/menu1.png', 450 + 58, 75 + 34);
	    
	    menu2 = createCenteredImageAtPosition('/images/menu2.png', 450 + 58, 143 + 34 - 1);

	    menu3 = createCenteredImageAtPosition('/images/menu3.png', 450 + 58, 210 + 34);


	    
	    
	    
	    soundText = addText(_('Sound: off'), 430, 75 + 10 + 11);
	    shuffleText = addText(_('Shuffle'), 450, 143 + 10 + 11);
		infoText = addText(_('Info'), 470, 210 + 10 + 11);


	    graphics.soundSettingsTextBox = soundText;
	    
	   
	    
	    
	    soundText.on('click', function (evt) {alterSoundEnabled(); });
	    menu1.on('click', function (evt) {alterSoundEnabled(); });

	    
	    shuffleText.on('click', function (evt) {shuffle(); });
	    menu2.on('click', function (evt) {shuffle(); });
	    
	    
	    infoText.on('click', function (evt) {showInfo(); });
	    menu3.on('click', function (evt) {showInfo(); });
	}


	function initScene() {
	    var stage = new createjs.Stage("gameCanvas");
	    graphics.stage = stage;
	}



	function initGraphics() {
	    graphics.$shortMessageDiv = $('#shortMessageDiv');
	    
	    createRefrigerator();

	    createPins();
	    
	    createLockHeader();
	    
	    createDoorHandle();

	    createMenu();
	}


    function setDescriptionText() {
        if (!graphics.field.isSolved) {
            graphics.$shortMessageDiv.text(_("Open all pins"));
            graphics.$shortMessageDiv.css('color', 'black');
        }
        
    }

    function initForInternetExplorerMobile() {
		// Don't know, why, but winPhone doesn't make a first render without this delay.
		var intrvl = setInterval(function() {
			clearInterval(intrvl);
			graphics.stage.update();
		}, 100);

		// It is already handled by css '-ms-touch-action: none; ', but here is one more check.
		// May be useful on other mobile devices, but wasn't checked.
		if (createjs.Touch.isSupported()) {
			createjs.Touch.initialize(graphics.stage);
		}
 


    }

	function init() {
		initScene();

		initGraphics();

		initField();
        
        
        
 
		setSoundStateText();
        
        setDescriptionText();


        initForInternetExplorerMobile();
	}


    
	graphics.pinsClosed = utils.createArray(field.size);
	graphics.pinsOpened = utils.createArray(field.size);
	graphics.switches = utils.createArray2d(field.size);
	graphics.$shortMessageDiv = null;
	graphics.field = field;


	graphics.init = init;
    
    

	return graphics;
}