/*jslint browser:true */
/*global $, jQuery*/
/*jslint nomen: true */
/*jslint plusplus: true */

"use strict";


/* 
    field - game logic of area with switches
    graphics - all UI + binding to game logic. The main class.
    dialogs - pop-ups with information + 'loading' message
    
    storage - save state to cookies
    audio - play and enable/disable sound
    utils - few methods about arrays + translation
    
    preloader - load all resources before game starts.
*/

$(document).ready(function () {
    var dialogs = createDialogs();
    
    dialogs.showLoadingMessage();
    
    
    
    var audio = createAudio(storage);
    
    var utils = createUtils();
    var storage = createStorage(utils);
    
    var field = createField(storage, utils);
    field.restore();
    
    
    var graphics = createGraphics(field, utils, dialogs, audio);
    
    var preloader = createPreloader();

    
    
    

    preloader.preloadData(function () {
        
        dialogs.hideLoadingMessage();
        
        graphics.init();
    });
});