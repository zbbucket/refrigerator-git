/*jslint browser:true */
/*global $, jQuery*/
/*jslint nomen: true */
/*jslint plusplus: true */

"use strict";

//==============================================================================
//                          Field
//==============================================================================

function createField(storage, utils) {
    var field = {},
        isStorageEnabled = !!storage,
        
        createArray,
        createArray2d;
    
    createArray = utils.createArray;
    createArray2d = utils.createArray2d;
    
    
    
    field.size = 4;
    field.switchStates = createArray2d(field.size);
    field.pinStates = createArray(field.size);
    field.isSolved = false;
    
    
    
    /*
        0-1-2-3-0-1-2-3.
        Returns true on overflow
    */
    field.rotateSingleSwitch = function (i, j) {
        var overflow = false,
            oldState = field.switchStates[i][j],
            newState;
    
        newState = (oldState + 1) % 4;
    
        field.switchStates[i][j] = newState;
    
        return newState < oldState;
    };
    
    field.checkIsFinished = function () {
        return field.isSolved;
    };
    
    // TODO: return array instead of callback
    field.rotateColAndRow = function (i, j, callbackOnSingleRotate) {
        var k;
        callbackOnSingleRotate = callbackOnSingleRotate || function () {};
    
        function rotateWithCallback(i, j) {
            var isOverflow = field.rotateSingleSwitch(i, j);
            callbackOnSingleRotate(i, j, isOverflow);
        }
        // 1. Rotate col and row, excluding clicked element.
        // Otherwise we get it rotated 2 times, and it will have same state.
        // The second bad thing that it will get additional animation.
        for (k = 0; k < field.size; k++) {
            if (k !== j) {
                rotateWithCallback(i, k);
            }
        }
        
        for (k = 0; k < field.size; k++) {
            if (k !== i) {
                rotateWithCallback(k, j);
            }
        }
        // 2. Rotate clicked element only once
        rotateWithCallback(i, j, callbackOnSingleRotate);
        
        field.updatePins();
    };
    
    
    field.isOpenedPinWithNumber = function (n) {
        var i;
        for (i = 0; i < field.size; i++) {
            if (field.switchStates[n][i] % 2 !== 0) {
                return false;
            }
        }
    
        return true;
    };
    
    field.updatePins = function () {
        var i, isSolved = true;
        for (i = 0; i < field.size; i++) {
            field.pinStates[i] = field.isOpenedPinWithNumber(i);
            isSolved = isSolved && field.pinStates[i];
        }
         
        field.isSolved = isSolved;
    };
    

    
    field.restore = function (size, state2dArray) {
        var i, j, v;
        
        field.switchStates = createArray2d(size);
        
        try {
            for (i = 0; i < size; i++) {
                for (j = 0; j < size; j++) {
                    v = state2dArray[i][j] % 4;
                    if (isNaN(v)) {throw new Error("Can't parse number"); }
                    
                    field.switchStates[i][j] = v;
                }
            }
            
            field.size = size;
            
            return true;
        } catch (e) {
            field.size = 4;
            field.switchStates = createArray2d(field.size);
            field.pinStates = createArray(field.size);
            return false;
        }
    };
    
    field.save = function () {
        if (!isStorageEnabled) {return; }
        storage.saveField(field.switchStates, field.size);
    };
    
    
    function initDefaultField() {
        var i;
        
        field.size = 4;
        field.switchStates = createArray2d(field.size);
        field.pinStates = createArray(field.size);
        
        for (i = 0; i < 5; i++) {
            field.shuffle();
        }
        // Otherwise if user loaded game and did nothing with switches,
        // he will get new fields every reload.
        field.save();
    }
    
    field.restore = function () {
        var i,
            storedData;
        
        if (!isStorageEnabled) {
            initDefaultField();
            return;
        }
        
        storedData = storage.loadField();
        
        if (storedData.isValid) {
            field.size = storedData.size;
            field.switchStates = storedData.field;
            field.pinStates = createArray(field.size);
        } else {
            initDefaultField();
        }
        
        field.updatePins();
    };
    
    
    
    
    function randomBoolean() {
        // chance of 'true' is 1:3, not a 1:1
        return Math.random() < 0.33;
    }
    

    
    
    field.shuffle = function (callbackOnSwitchRotate) {
		var i, j, k;
		callbackOnSwitchRotate = callbackOnSwitchRotate || function () {};

		for (i = 0; i < field.size; i++) {
		    for (j = 0; j < field.size; j++) {
		        if (randomBoolean()) {
                    field.rotateColAndRow(i, j, callbackOnSwitchRotate);
		        }
		    }
		}

        
        field.updatePins();
    };
    
  
    
    
    return field;
}