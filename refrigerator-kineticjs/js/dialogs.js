/*jslint browser: true*/
/*global $, jQuery, alert*/
//"use strict";

function showInfoWindow() {
    $("#infoDiv").dialog({modal: true, title: 'Досье', minWidth: 620, minHeight: 550});
}


function showYoutubeWindow() {
    $('#priseLink').click();
}

function initYoutubeLink() {
    var titleText = 'Приз',
        text = $('#priseDivText').html();
    
    
    $(function () {
        $("a.youtube").YouTubePopup({ autoplay: 0, headerText: text, title: titleText, start: 152});
    });
}


$(document).ready(function () {
//    storeState();
    initYoutubeLink();

    $('#infoButton').click(function (evt) {
        showInfoWindow();
    });
});


