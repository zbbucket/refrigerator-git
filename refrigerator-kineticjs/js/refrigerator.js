/*jslint browser:true */
/*global $, jQuery, alert, Kinetic*/
/*jslint nomen: true */
/*jslint plusplus: true */

//"use strict";


// 1. Field size
var size = -1; // see setInitialState



// 2. Switches on the gameboard
var switches = null; // see setInitialState

// 3. Is it first run
var isStateRestoredFromCookies = false;
var fieldFromCookie = null;


// JSLint will warn only this function


function createArray(size) {
    return new Array(size);
}


function loadStateFromCookie() {
    $.cookie.json = true;
    
    var size = $.cookie('fieldSize'),
        storedFieldState = $.cookie('fieldState'),
        resultFieldState = null,
        i,
        j,
        v;
   
    if (typeof (size) === 'undefined' || size < 1 || size > 10) {
        return [false, -1, null];
    }
    
    
    try {
        resultFieldState = createArray(size);
    
        for (i = 0; i < size; i++) {
            resultFieldState[i] = createArray(size);
            for (j = 0; j < size; j++) {
                v = parseInt(storedFieldState[i][j], 10);
                resultFieldState[i][j] = v;
                
                if (isNaN(v)) {throw new Error('Cookie has invalid data'); }
            }
        }
    } catch (e) {
        // Cookie has invalid data, can't use it
        return [false, -1, null];
    }
        
        
    return [true, size, resultFieldState];
}

function saveStateToCookie() {
    $.cookie.json = true;
    
    $.cookie('fieldSize', size);
    
    var i,
        j,
        data = createArray(size);
    
    for (i = 0; i < size; i++) {
        data[i] = createArray(size);
        for (j = 0; j < size; j++) {
            data[i][j] = switches[i][j].stateAngle;
        }
    }
    
    
    $.cookie('fieldState', data);
}





// TODO: createArrayForSwitches
function initSwitchesArray() {
    var i;
    
    switches = createArray(size);
    
    for (i = 0; i < size; i++) {
        switches[i] = createArray(size);
    }
}


function setInitialState() {
    var cookieState = loadStateFromCookie();
    // 1. Can't properly read cookie.
    if (cookieState[0] === false) {
        size = 4;
        initSwitchesArray();
        isStateRestoredFromCookies = false;
    } else {
        size = cookieState[1];
        initSwitchesArray();
        isStateRestoredFromCookies = true;
        fieldFromCookie = cookieState[2];
    }
}



setInitialState();



// 4. KineticJS objects
var _soundSettingsKineticTextBox = null;
var pinsOpened = createArray(size);
var pinsClosed = createArray(size);

// 5. Other UI

var $shortMessageDiv = null;

//=============================================================================
//                           Messages
//=============================================================================

var translation = {
    'Done' : 'Готово! Дерните за рычаг, чтобы открыть дверь',
    'Not solved yet' : 'Заперто',
    'Open all pins' : 'Откройте все штифты',
    
    'Sound: off': 'Звук: выкл.',
    'Sound: on': 'Звук: вкл.',
    'Shuffle': 'Смешать',
    'Info': 'Досье'
};

function _(s) {
    var res = translation[s];

    if (res === null) {
        res = s;
    }
    
    return res;
}

//=============================================================================
//                          Audio
//=============================================================================
// Is browser supports audio?
var _isSoundAvailable = false;
// Is user wants audio?
var _isSoundEnabled = false;

function isSoundEnabled() {return _isSoundEnabled && _isSoundAvailable; }


var _audioReferences = [
    'clickAudio',
    'shuffleAudio',
    'closedDoorAudio',
    'openedDoorAudio',
    'latchAudio'];

var _numberOfAudioFiles = _audioReferences.length;

// HTML audio can't play two files at once. If user clicked very fast,
// then we need a few audio elements with same file. That is audioStack.

var _audioStacks = {}; // ex: map name 'clickAudio' to array(5) of 'audio' elements
var _audioStackCounters = {}; // ex: map name 'clickAudio' to index of last used element

var _audioStackSize = 5;


function initAudio() {
    var i, j, audio;
    for (i = 0; i < _numberOfAudioFiles; i++) {
        // 1. Make storage for each audio file
        audio = _audioReferences[i];
        _audioStackCounters[audio] = 0;
        _audioStacks[audio] = createArray(_audioStackSize);
        
        // 2. Fill storage with cloned objects
        for (j = 0; j < _audioStackSize; j++) {
            _audioStacks[audio][j] = document.getElementById(audio).cloneNode(true);
        }
    }
    // All sound objects are available, sound is OK.
    _isSoundAvailable = true;
}


function play(file) {
    if (!isSoundEnabled()) {return; }
    
    
    if (!(_audioStacks.hasOwnProperty(file)) || !(_audioStackCounters.hasOwnProperty(file))) {
        // Unknown file, we don't have it audio objects.
        return;
    } else {
        try {
            // 1. Go to next available player object
            var counter = _audioStackCounters[file];
            counter = (counter + 1) % _audioStackSize;
            _audioStackCounters[file] = counter;
            
            // 2. Play data with it.
            var audioObject = _audioStacks[file][counter]
            
            // If we don't rewind already-used element, we can have sound problems.
            audioObject.pause();
            audioObject.currentTime = 0;
            audioObject.play();    
        } catch (e) {
            // sometimes we can't play the audio. Presumable, because it is not loaded yet.
        }
        
    }
}

function setSoundStateText() {
    var newText, textObject;
    if (isSoundEnabled()) {
        newText = _('Sound: on');
    } else {
        newText = _('Sound: off');
    }
    
    textObject = _soundSettingsKineticTextBox;
    textObject.text(newText);
    textObject.getLayer().draw();
}

function setSoundEnabled(isEnabled) {
    _isSoundEnabled = isEnabled;
    
    setSoundStateText();
}

//=============================================================================
//                        Callbacks
//=============================================================================


function showInfo() {
    showInfoWindow();
}

function prise() {
    showYoutubeWindow();
}

function alterSoundEnabled() {
    setSoundEnabled(!isSoundEnabled());
}

// see also 'shuffle()'

//=============================================================================
//                          State of locks
//=============================================================================

function isOpenedPinWithNumber(n) {
    var i;
    for (i = 0; i < size; i++) {
        if (switches[n][i].stateAngle % 2 !== 0) {
            return false;
        }
    }

    return true;
}

function updateGraphicsOfPinWithNumber(n) {
    var pin = pinsClosed[n];
    
    if (isOpenedPinWithNumber(n)) {
        pin.hide();
    } else {
        pin.show();
    }
}

function checkIsFinished() {
    var i, isOpened;
    for (i = 0; i < size; i++) {
        isOpened = isOpenedPinWithNumber(i);
        if (!isOpened) {
            return false;
        }
    }
    
    return true;
}


function updateLockStatusText() {
    var isDone = checkIsFinished();
    if (isDone) {
        $shortMessageDiv.text(_("Done"));
        $shortMessageDiv.css('color', 'green');
    } else {
        $shortMessageDiv.text(_("Not solved yet"));
        $shortMessageDiv.css('color', 'red');
    }
}

function updateLocks() {
    var i;
    for (i = 0; i < size; i++) {
        updateGraphicsOfPinWithNumber(i);
    }
     
    updateLockStatusText();
}




//=============================================================================
//                                  Rotation
//=============================================================================

function rotate(image, isAnimated) {
    var sw = image;
    
    sw.stateAngle += 1;
    
    // Do not go over 360 degrees, just in case
    if (sw.stateAngle > 4) {
        sw.stateAngle -= 4;
        sw.rotate(-360);
    }
    
    if (isAnimated) {
        sw.tween = new Kinetic.Tween({
            node: sw,
            duration: 0.3,
            rotationDeg: sw.stateAngle * 90,
            easing: Kinetic.Easings.EaseOut
        });
        sw.tween.play();

    } else {
        sw.rotate(90, false);
    }
}

function rotateRowAndCol(i, j, isAnimated) {
    var k;
    // 1. Rotate col and row, excluding clicked element.
    // Otherwise we get it rotated 2 times, and it will have same state.
    // The second bad thing that it will get additional animation.
    for (k = 0; k < size; k++) {
        if (k !== j) {
            rotate(switches[i][k], isAnimated);
        }
    }
    
    for (k = 0; k < size; k++) {
        if (k !== i) {
            rotate(switches[k][j], isAnimated);
        }
    }
    // 2. Rotate clicked element only once
    rotate(switches[i][j], isAnimated);
}




//=============================================================================
//                              Shuffle
//=============================================================================


function randomBoolean() {
    // chance of 'true' is 1:3, not a 1:1
    return Math.random() < 0.33;
}

function randomRotateField(isAnimated) {
    var i, j;
    for (i = 0; i < size; i++) {
        for (j = 0; j < size; j++) {
            if (randomBoolean()) {
                rotateRowAndCol(i, j, isAnimated);
            }
        }
    }
    
    saveStateToCookie();
}

function doShuffle(isAnimated) {
    randomRotateField(isAnimated);
    
    updateLocks();
}

var _isShuffling = false;
var _shuffleSteps = 5;


function shuffle() {
    // Prevent audio stack overflow
    if (_isShuffling) {return; }
    _isShuffling = true;
    
    
    var timesToShuffle = _shuffleSteps,
        interval;
    
    // 1. Start a long-playing sound effect.
    play('shuffleAudio');


    // 2. Start first shuffle animation.
    timesToShuffle -= 1;
    doShuffle(true);
    


    interval = setInterval(function () {
        // 3. Animate shuffle every 80 ms.
        timesToShuffle -= 1;
        doShuffle(true);
        
        // 4. Repeat it only (_shuffleSteps-1) times       
        if (timesToShuffle < 0) {
            clearInterval(interval);
            saveStateToCookie();
            _isShuffling = false;
        }
        
        
    }, 80);
}


function shuffleWithoutAnimation() {
    var i;
    for (i = 0; i < _shuffleSteps; i++) {
        doShuffle(false);
    }
}

function restoreStateFromCookieData() {
    var i = 0,
        j = 0,
        v;
    
    
    for (i = 0; i < size; i++) {
        for (j = 0; j < size; j++) {
            v = fieldFromCookie[i][j];
            if (v < 0 || v > 10) {v = 0; } // ignore invalid data
            
            // Two states is enough for game logic. However, switches have
            // shadows, so 90 and 270 degrees are different in UI.
            for (v; v > 0; v--) {
                rotate(switches[i][j], false);
            }
        }
    }
    
    updateLocks();
}


//=============================================================================
//                           Kinetic methods
//=============================================================================


function kineticFromImage(img) {
    var dx = img.width / 2,
        dy = img.height / 2,
        kineticImage = new Kinetic.Image({
            x: dx,
            y: dy,
            image: img,
            width: img.width,
            height: img.height,
            
            offset: {x: dx, y: dy}
        });
    
    return kineticImage;
}

function addText(layer, text) {
    var simpleText = new Kinetic.Text({
        x: 15,
        y: 15,
        text: text,
        fontSize: 25,
        fontFamily: 'Calibri',
        fill: 'green'
    });
    
    
          // to align text in the middle of the screen, we can set the
      // shape offset to the center of the text shape after instantiating it
    simpleText.offsetX(simpleText.width() / 2);
    
    layer.add(simpleText);
    
    return simpleText;
}

function moveKineticObjectToPosition(kineticImage, x, y) {
    kineticImage.x(x + kineticImage.width() / 2);
    kineticImage.y(y + kineticImage.height() / 2);
}

function createSwitch(i, j, images) {
    var sw = kineticFromImage(images['/images/switch.png']);

    // Model data
    sw.stateAngle = 0;
    sw.i = i;
    sw.j = j;
            
            
    moveKineticObjectToPosition(sw, 70 + i * 40 , 145 + j * 40 );
    
    
    switches[i][j] = sw;
    
    
    sw.on('click', function (evt) {
        rotateRowAndCol(sw.i, sw.j, true);
        updateLocks();
        
        saveStateToCookie();
        
        if (checkIsFinished()) {
            play('latchAudio');
        } else {
            play('clickAudio');
        }
    });

    return sw;
}

function initField(layer, images) {
    var i, j, sw;
    for (i = 0; i < size; i++) {
        for (j = 0; j < size; j++) {
            sw = createSwitch(i, j, images);
            layer.add(sw);
        }
    }
}


function addPins(layer, images) {
    var i, pin;
    for (i = 0; i < size; i++) {
        pinsClosed[i] = kineticFromImage(images['/images/pin_closed.png']);
        pinsOpened[i] = kineticFromImage(images['/images/pin_opened.png']);
    }
    
    // Pin has two images, full/shorted for closed/opened state.
    // Normally the 'full' image overshadows 'shorted' one, and we
    // see a long pin in a lock. When it is opened, the 'full' image is hidden,
    // and we see only a short stick that don't prevent the door opening.
    for (i = 0; i < size; i++) {
        pin = pinsOpened[i];
        moveKineticObjectToPosition(pin, 100 + 29 * i + 9, 50 + 46 - 1);
        layer.add(pin);
    }
    
    for (i = 0; i < size; i++) {
        pin = pinsClosed[i];
        moveKineticObjectToPosition(pin, 100 + 29 * i + 9, 50 - 2);
        
        layer.add(pin);
    }
}

function addDoorHandle(layer, images) {
    var hook = kineticFromImage(images['/images/hook.png']);
    moveKineticObjectToPosition(hook, 17, 182);
    
    hook.on('click', function (evt) {
        if (checkIsFinished()) {
            
            play('openedDoorAudio');
            
            prise();
        
        } else {
            play('closedDoorAudio');
        }
        
    });
    
    
    layer.add(hook);
}

function initMenu(layer, images) {
    var menu1, menu2, menu3,
        soundText, shuffleText, infoText;
    menu1 = kineticFromImage(images['/images/menu1.png']);
    moveKineticObjectToPosition(menu1, 400, 75);
    layer.add(menu1);
    
    menu2 = kineticFromImage(images['/images/menu2.png']);
    moveKineticObjectToPosition(menu2, 400, 143);
    layer.add(menu2);

    menu3 = kineticFromImage(images['/images/menu3.png']);
    moveKineticObjectToPosition(menu3, 400, 210);
    layer.add(menu3);

    
    
    
    soundText = addText(layer, _('Sound: off'));
    moveKineticObjectToPosition(soundText, 430, 75 + 10);
    
    _soundSettingsKineticTextBox = soundText;
    
    shuffleText = addText(layer, _('Shuffle'));
    moveKineticObjectToPosition(shuffleText, 450, 143 + 10);
    
    infoText = addText(layer, _('Info'));
    moveKineticObjectToPosition(infoText, 470, 210 + 10);
    
    
    
    
    
    
    soundText.on('click', function (evt) {alterSoundEnabled(); });
    menu1.on('click', function (evt) {alterSoundEnabled(); });

    
    shuffleText.on('click', function (evt) {shuffle(); });
    menu2.on('click', function (evt) {shuffle(); });
    
    
    infoText.on('click', function (evt) {showInfo(); });
    menu3.on('click', function (evt) {showInfo(); });
}



function addLockHeader(layer, images) {
    var header = kineticFromImage(images['/images/lock_header.png']);
    moveKineticObjectToPosition(header, 88 + 12, 20 + 2);
    
    layer.add(header);
}


function initKineticAfterImagesLoad(images) {
    var layer, refrigeratorImage, stage;
    
    layer = new Kinetic.Layer();
    
    // Otherwise Windows Phone IE will be unable to click on elements
    Kinetic.UA.mobile = false;

    // 1. Fridge itself. It size defines whole stage dimensions
    refrigeratorImage = kineticFromImage(images['/images/refrigerator.png']);
    layer.add(refrigeratorImage);
        
    stage = new Kinetic.Stage({
        container: 'container',
        // 250 is for left menu size
        width: refrigeratorImage.width() + 250,
        height: refrigeratorImage.height()
    });

    // 2. Pins. Each pin has 'opened' and 'closed' images

    addPins(layer, images);
    
    

    // 3. Lock Header. It is added after pins, so it will overshadow their top edges.
    
    addLockHeader(layer, images);
    
    // 4. Hook to open door after puzzle is solved.
    addDoorHandle(layer, images);
    
    // 5. Add switches on the game board
    initField(layer, images);
    
    
    // 6. Menu to the left
    initMenu(layer, images);
    
    // 7. Finally show our game.
    stage.add(layer);
}

//=============================================================================
//                                    Entry point
//=============================================================================

function initGameFieldState() {
    if (isStateRestoredFromCookies) {
        restoreStateFromCookieData();
    } else {
        shuffleWithoutAnimation();
        saveStateToCookie();
    }
    
    
    $shortMessageDiv.text(_("Open all pins"));
    $shortMessageDiv.css('color', 'black');
}

var _images = {};

var _imageLocations = [
    '/images/refrigerator.png',
    '/images/lock_header.png',
    '/images/switch.png',
    '/images/pin_opened.png',
    '/images/pin_closed.png',
    '/images/hook.png',
    '/images/menu1.png',
    '/images/menu2.png',
    '/images/menu3.png'];


var _loadedImagesCount = 0;

function imageLoaded() {
    _loadedImagesCount += 1;
    if (_loadedImagesCount < _imageLocations.length) {
        return;
    }


    initKineticAfterImagesLoad(_images);

    initGameFieldState();

    initAudio();
    
    setSoundEnabled(true);
}

function startApplication() {
    // Wait for all resources, and then launch application

    _imageLocations.map(function (addr) {
        var imageObject = new Image();
        
        imageObject.onload = function () {
            _images[addr] = imageObject;
            imageLoaded();
        };
        
        imageObject.src = addr;
    });
}



$(document).ready(function () {
    $shortMessageDiv = $('#shortMessageDiv');

    startApplication();
});
